# cabify-shopping-cart

## Notes

### In main.js a Checkout instance is created. A "global"/outer-scope object is needed to play the "Store" role. I did not use Flux/Vuex, for smaller project size.

### The Checkout class path is src/common/Checkout.js

### The discount functionality is very scalable. At Discount.js there are the discount functions. Each discount/function is not binded to a specific product. Each discount can be applied to every product.  

### I have added links for adding products to the Cart.

### DummyData refer to products and discount rules (data that should be retrieved from a database with ajax call)

### Main.js refers to the first execution of javascript code. App.Vue is the root component that is instantitated into main.js.

### A lot of code improvements could have taken place, like using tests or typescript.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

```

### Lints and fixes files
```
npm run lint
```
