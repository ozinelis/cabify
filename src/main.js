import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import Checkout from "@/common/Checkout";
import { products, discountRules } from "@/common/dummyData";

createApp(App)
  .use(router)
  .mount("#app");

export default new Checkout({
  productsList: JSON.parse(JSON.stringify(products)),
  pricingRules: discountRules
});
