export default {
  twoForOne({ products, discountProduct }) {
    const product = products[discountProduct.product.id];
    if (!product) return undefined;

    const productCount = product.count;
    let discount = 0;
    if (productCount > 0) {
      const timesDiscountApplied = Math.trunc(productCount / 2);
      discount = timesDiscountApplied * discountProduct.product.price;
    }
    return {
      discountValue: discount,
      discountProductId: discountProduct.product.id,
      discountProductDescription: discountProduct.product.description,
      discountDescription: "2x1",
      discountId: "twoForOne"
    };
  },

  fivePercentOnThreeOrMore({ products, discountProduct }) {
    const product = products[discountProduct.product.id];
    if (!product) return undefined;

    const productCount = product.count;
    return {
      discountValue:
        productCount >= 3
          ? productCount * discountProduct.product.price * 0.05
          : 0,
      discountProductId: discountProduct.product.id,
      discountProductDescription: discountProduct.product.description,
      discountDescription: "x3",
      discountId: "fivePercentOnThreeOrMore"
    };
  }
};
