import Discounts from "./Discount";
import { products as dummyProductsList } from "@/common/dummyData";
//The methods/properties names are self-explanatory.
class Checkout {
  constructor({ productsList, pricingRules }) {
    //Properties are declared into constructor because they should not be mutated without the use of a class method.
    this.discountObjs = [];
    this.discounts;
    this.totalValue;
    this.totalNoDiscountValue;
    this.totalDiscountValue;
    this.productsQuantity;

    this.productsList = productsList;
    this.pricingRules = pricingRules;
    this.init();
  }

  scan(productId) {
    const product = this.productsList[productId];
    if (!product) {
      this.productsList[productId] = dummyProductsList[productId];
      this.productsList[productId].count = 0;
    }
    this.productsList[productId].count++;
    this.init();
  }

  remove(productId) {
    this.productsList[productId].count--;

    if (this.productsList[productId].count === 0)
      delete this.productsList[productId];

    this.init();
  }

  setQuantity(productId, quantity) {
    if (quantity < 0) this.productsList[productId].count = 0;

    if (quantity == 0 || this.productsList[productId].count == 0) {
      this.productsList[productId].count = 0;
      delete this.productsList[productId];
    }

    this.init();
  }

  getProductsQuanity() {
    this.productsQuantity = 0;
    for (const property in this.productsList) {
      const object = this.productsList[property];
      const count = parseInt(object.count);
      this.productsQuantity += count;
    }
    return this.productsQuantity;
  }

  getDiscounts() {
    this.discountObjs = [];
    for (const rule of this.pricingRules) {
      const discountObj = Discounts[rule.discountId]({
        products: this.productsList,
        discountProduct: rule.discountProduct
      });

      if (discountObj) this.discountObjs.push(discountObj);
    }
  }

  totalDiscount() {
    const reducer = (sum, val) => sum + val.discountValue;
    const initialValue = 0;
    this.totalDiscountValue = this.discountObjs.reduce(reducer, initialValue);
    return this.totalDiscountValue;
  }

  totalNoDiscount() {
    this.totalNoDiscountValue = 0;
    for (const property in this.productsList) {
      const object = this.productsList[property];
      const value = object.product.price;
      this.totalNoDiscountValue += value * object.count;
    }
    return this.totalNoDiscountValue;
  }

  total() {
    this.getDiscounts();
    this.totalValue = this.totalNoDiscount() - this.totalDiscount();
    return this.totalValue;
  }

  init() {
    this.getProductsQuanity();
    this.getDiscounts();
    this.total();
  }
}

export default Checkout;
