const products = {
  TSHIRT: {
    product: {
      id: "TSHIRT",
      name: "Cabify T-Shirt",
      description: "Shirt",
      price: 20,
      code: "X7R2OPX",
      img: "assets/products/shirt.png",
      alt: "Shirt",
      longDescription: `Lorem ipsum dolor sit amet, consectetur
      adipiscing elit. In sodales semper elit sit amet
      interdum. Praesent volutpat sed elit vel
      consectetur. Nulla tempus tincidunt ex, sit
      amet semper ipsum imperdiet varius. In
      rutrum aliquam nisl, sagittis faucibus felis
      bibendum id.`
    },
    count: 3
  },
  MUG: {
    product: {
      id: "MUG",
      name: "Cabify Coffee Mug",
      description: "Mug",
      price: 5,
      code: "X2G2OPZ",
      img: "assets/products/mug.png",
      alt: "Mug",
      longDescription: `Lorem ipsum dolor sit amet, consectetur
      adipiscing elit. In sodales semper elit sit amet
      interdum. Praesent volutpat sed elit vel
      consectetur. Nulla tempus tincidunt ex, sit
      amet semper ipsum imperdiet varius. In
      rutrum aliquam nisl, sagittis faucibus felis
      bibendum id.`
    },
    count: 4
  },
  CAP: {
    product: {
      id: "CAP",
      name: "Cabify Cap",
      description: "Cap",
      price: 10,
      code: "X3W2OPY",
      img: "assets/products/cap.png",
      alt: "Cap",
      longDescription: `Lorem ipsum dolor sit amet, consectetur
      adipiscing elit. In sodales semper elit sit amet
      interdum. Praesent volutpat sed elit vel
      consectetur. Nulla tempus tincidunt ex, sit
      amet semper ipsum imperdiet varius. In
      rutrum aliquam nisl, sagittis faucibus felis
      bibendum id.`
    },
    count: 4
  }
};

const discountRules = [
  {
    discountId: "twoForOne",
    discountProduct: products.MUG
  },
  {
    discountId: "fivePercentOnThreeOrMore",
    discountProduct: products.TSHIRT
  }
];

export { products, discountRules };
